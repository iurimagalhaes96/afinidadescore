package br.com.serasa.score.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.serasa.score.dto.AfinidadeDTO;
import br.com.serasa.score.service.AfinidadeService;

@RestController
@RequestMapping("afinidade")
public class AfinidadeController {
	
	
	private AfinidadeService afinidadeService;
	
	public AfinidadeController(AfinidadeService afinidadeService) {
		this.afinidadeService = afinidadeService;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public AfinidadeDTO cadastrarAfinidade(
		@RequestBody AfinidadeDTO afinidadeDTO) {
		return afinidadeService.cadastrarAfinidade(afinidadeDTO);
	}
}
