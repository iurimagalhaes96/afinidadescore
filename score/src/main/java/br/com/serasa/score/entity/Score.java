package br.com.serasa.score.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.serasa.score.dto.ScoreDTO;

@Entity
@Table(name ="TB_SCORE")
public class Score {
	
	@Id
	@Column
	private String descricao;
	@Column
	private Integer inicio;
	@Column
	private Integer fim;

	public Score(ScoreDTO scoreDTO) {
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getInicio() {
		return inicio;
	}

	public void setInicio(Integer inicio) {
		this.inicio = inicio;
	}

	public Integer getFim() {
		return fim;
	}

	public void setFim(Integer fim) {
		this.fim = fim;
	}

	@Override
	public int hashCode() {
		return Objects.hash(descricao, fim, inicio);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Score other = (Score) obj;
		return Objects.equals(descricao, other.descricao) && Objects.equals(fim, other.fim)
				&& Objects.equals(inicio, other.inicio);
	}
	
}
