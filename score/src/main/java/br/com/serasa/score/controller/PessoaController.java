package br.com.serasa.score.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.serasa.score.dto.NovaPessoaDTO;
import br.com.serasa.score.dto.PessoaDTO;
import br.com.serasa.score.service.PessoaService;

@RestController
@RequestMapping("pessoa")
public class PessoaController {
	
	
	private PessoaService pessoaService;
	
	public PessoaController(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	
	@GetMapping
	public List<PessoaDTO> getPessoas(){
		return pessoaService.listarPessoas();
	}
	
	@GetMapping("{id}")
	public PessoaDTO getPessoaById(
			@PathVariable(name = "id") Long id) {
		return pessoaService.buscarPessoaPorId(id);
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public PessoaDTO cadastrarPessoa(
		@RequestBody NovaPessoaDTO novaPessoaDTO) {
		return pessoaService.cadastrarPessoa(novaPessoaDTO);
		
	}
}
