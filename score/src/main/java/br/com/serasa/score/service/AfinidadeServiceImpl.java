package br.com.serasa.score.service;

import org.springframework.stereotype.Service;

import br.com.serasa.score.dto.AfinidadeDTO;
import br.com.serasa.score.entity.Afinidade;
import br.com.serasa.score.repository.AfinidadeRepository;

@Service
public class AfinidadeServiceImpl implements AfinidadeService {
	
	private AfinidadeRepository afinidadeRepository;
	
	private AfinidadeServiceImpl(AfinidadeRepository afinidadeRepository) {
		this.afinidadeRepository = afinidadeRepository;
	}

	@Override
	public AfinidadeDTO cadastrarAfinidade(AfinidadeDTO afinidadeDTO) {
		Afinidade afinidade= new Afinidade(afinidadeDTO);
		Afinidade afinidadeCadastrada = afinidadeRepository.save(afinidade);
		return new AfinidadeDTO(afinidadeCadastrada);
	}

}
