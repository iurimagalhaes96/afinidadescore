package br.com.serasa.score.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serasa.score.entity.Score;

public interface ScoreRepository extends JpaRepository<Score, Long>{

}
