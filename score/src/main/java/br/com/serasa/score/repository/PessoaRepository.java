package br.com.serasa.score.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serasa.score.entity.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long>{
	
}
