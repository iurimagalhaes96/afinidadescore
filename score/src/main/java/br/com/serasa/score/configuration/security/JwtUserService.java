package br.com.serasa.score.configuration.security;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.serasa.score.repository.UserRepository;

@Service
public class JwtUserService implements UserDetailsService{

	private UserRepository userRepository;
	
	public JwtUserService(UserRepository userRepository) { this.userRepository = userRepository;}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findFirstByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("Usuário %s não encontrado", username));
		}
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(), 
				user.getPassword(), 
				new ArrayList<>());
	}
	
	

}
