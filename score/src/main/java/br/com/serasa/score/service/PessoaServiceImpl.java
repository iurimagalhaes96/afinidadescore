package br.com.serasa.score.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import br.com.serasa.score.dto.NovaPessoaDTO;
import br.com.serasa.score.dto.PessoaDTO;
import br.com.serasa.score.entity.Pessoa;
import br.com.serasa.score.repository.PessoaRepository;

@Service
public class PessoaServiceImpl implements PessoaService {
	
	private PessoaRepository pessoaRepository;
	
	private PessoaServiceImpl(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}

	@Override
	public List<PessoaDTO> listarPessoas() {
		List<Pessoa> pessoaList = pessoaRepository.findAll();
		
		return pessoaList
				.stream()
				.map(PessoaDTO::new)
				.collect(Collectors.toList());
	}

	@Override
	public PessoaDTO buscarPessoaPorId(Long id) {
		Pessoa pessoa = null;
		try {
			pessoa = pessoaRepository.findById(id).orElseThrow(NotFoundException::new);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return new PessoaDTO(pessoa);
	}


	@Override
	public PessoaDTO cadastrarPessoa(NovaPessoaDTO novaPessoaDTO) {
		Pessoa pessoa = new Pessoa(novaPessoaDTO);
		Pessoa pessoaCadastrada = pessoaRepository.save(pessoa);
		return new PessoaDTO(pessoaCadastrada);
	}

}
