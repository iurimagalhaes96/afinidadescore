package br.com.serasa.score.service;

import br.com.serasa.score.dto.ScoreDTO;

public interface ScoreService {
	
	ScoreDTO cadastrarScore(ScoreDTO scoreDTO);

}
