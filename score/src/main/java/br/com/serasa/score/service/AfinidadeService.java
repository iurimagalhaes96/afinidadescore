package br.com.serasa.score.service;

import br.com.serasa.score.dto.AfinidadeDTO;

public interface AfinidadeService {
	
	AfinidadeDTO cadastrarAfinidade(AfinidadeDTO afinidadeDTO);

}
