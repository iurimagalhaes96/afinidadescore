package br.com.serasa.score.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.serasa.score.dto.ScoreDTO;
import br.com.serasa.score.service.ScoreService;

@RestController
@RequestMapping("score")
public class ScoreController {

	private ScoreService scoreService;

	public ScoreController(ScoreService scoreService) {
		this.scoreService = scoreService;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ScoreDTO cadastrarScore(@RequestBody ScoreDTO scoreDTO) {
		return scoreService.cadastrarScore(scoreDTO);
	}
}
