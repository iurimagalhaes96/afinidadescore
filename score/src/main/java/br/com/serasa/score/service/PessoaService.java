package br.com.serasa.score.service;

import java.util.List;

import br.com.serasa.score.dto.NovaPessoaDTO;
import br.com.serasa.score.dto.PessoaDTO;

public interface PessoaService {
	
	List<PessoaDTO> listarPessoas();
	PessoaDTO buscarPessoaPorId(Long id);
	PessoaDTO cadastrarPessoa(NovaPessoaDTO novaPessoaDTO);

}
