package br.com.serasa.score.entity;

import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.serasa.score.dto.AfinidadeDTO;

@Entity
@Table(name ="TB_AFINIDADE")
public class Afinidade {
	
	@Id
	@Column
	private String regiao;
	@ElementCollection
	private List<String> estados;

	public Afinidade(AfinidadeDTO afinidadeDTO) {
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public List<String> getEstados() {
		return estados;
	}

	public void setEstados(List<String> estados) {
		this.estados = estados;
	}

	@Override
	public int hashCode() {
		return Objects.hash(estados, regiao);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Afinidade other = (Afinidade) obj;
		return Objects.equals(estados, other.estados) && Objects.equals(regiao, other.regiao);
	}
	
}
