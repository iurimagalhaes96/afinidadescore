package br.com.serasa.score.dto;

import java.util.List;

import br.com.serasa.score.entity.Afinidade;

public class AfinidadeDTO {

	private String regiao;
	private List<String> estados;
	
	public AfinidadeDTO(Afinidade afinidadeCadastrada) {
	}
	public String getRegiao() {
		return regiao;
	}
	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}
	public List<String> getEstados() {
		return estados;
	}
	public void setEstados(List<String> estados) {
		this.estados = estados;
	}
	
}
