package br.com.serasa.score.dto;

import br.com.serasa.score.entity.Score;

public class ScoreDTO {

	private String scoreDescricao;
	private Integer inicio;
	private Integer fim;
	
	public ScoreDTO(Score scoreCadastrado) {
	}
	public String getScoreDescricao() {
		return scoreDescricao;
	}
	public void setScoreDescricao(String scoreDescricao) {
		this.scoreDescricao = scoreDescricao;
	}
	public Integer getInicio() {
		return inicio;
	}
	public void setInicio(Integer inicio) {
		this.inicio = inicio;
	}
	public Integer getFim() {
		return fim;
	}
	public void setFim(Integer fim) {
		this.fim = fim;
	}
	
}
