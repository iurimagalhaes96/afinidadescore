package br.com.serasa.score.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.serasa.score.entity.Afinidade;

public interface AfinidadeRepository extends JpaRepository<Afinidade, Long>{

}
