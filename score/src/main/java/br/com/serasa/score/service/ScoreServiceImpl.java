package br.com.serasa.score.service;

import org.springframework.stereotype.Service;

import br.com.serasa.score.dto.ScoreDTO;
import br.com.serasa.score.entity.Score;
import br.com.serasa.score.repository.ScoreRepository;

@Service
public class ScoreServiceImpl implements ScoreService {
	
	private ScoreRepository scoreRepository;
	
	private ScoreServiceImpl(ScoreRepository scoreRepository) {
		this.scoreRepository = scoreRepository;
	}

	@Override
	public ScoreDTO cadastrarScore(ScoreDTO scoreDTO) {
		Score score = new Score(scoreDTO);
		Score scoreCadastrado = scoreRepository.save(score);
		return new ScoreDTO(scoreCadastrado);
	}

}
